<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'users'], function () use ($router) {
    $router->post('/data', 'UsersController@data');
    $router->post('/login', 'UsersController@login');
    $router->post('/register', 'UsersController@register');
    $router->delete('/{id}', 'UsersController@hapus');
});

$router->group(['prefix' => 'pasien'], function () use ($router) {
    $router->get('/aktif', 'PasienController@aktif');
    $router->get('/aktifkedua', 'PasienController@aktifkedua');
    $router->get('/data', 'PasienController@data');
    $router->get('/datapasienmasuk', 'PasienController@dataPasienMasuk');
    $router->get('/data/{id}', 'PasienController@pasien');
    $router->put('/ganti', 'PasienController@ganti');
    $router->delete('/{id}', 'PasienController@hapus');
    $router->post('/catatan', 'PasienController@catatan');
    $router->get('/catatan/{id}', 'PasienController@catatanById');
    $router->get('/laporan/{id}', 'PasienController@laporanById');
    $router->get('/sensor/{id}', 'PasienController@sensorById');
    $router->get('/sensor/chart/{id}', 'PasienController@sensorChartById');
    $router->get('/sensor/data/{id}/{alat}', 'PasienController@dataSensor');
    $router->post('/sensor', 'PasienController@sensor');
    $router->post('/register', 'PasienController@register');
});

$router->group(['prefix' => 'akses'], function () use ($router) {
    $router->get('/data', function()
    {
        $data = DB::table('tb_akses')->get();
        return $data;
    });
});
