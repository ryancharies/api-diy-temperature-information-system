<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use DB;

class UsersController extends Controller
{
    public function data(Request $request)
    {
        $param = $request->json()->all();
        $users = DB::table('tb_users as u')
                    ->join('tb_akses as a','u.id_akses','=','a.id')
                    ->select('u.*','a.keterangan as akses');

        if($param['id'] > 0){
            $users->where('u.id', $param['id']);
        }

        return $users->get();
    }

    public function register(Request $request)
    {
        $req = $request->json()->all();
        $req['password'] = md5($req['password']);

        $users = DB::table('tb_users')->insert($req);

        if(!$users){
            $status = 201;
            $msg = 'failed';
        }else{
            $status = 200;
            $msg = 'success';
        }

        return response()->json($msg, $status);
    }

    public function login(Request $request)
    {
        $req = $request->json()->all();
        $req['password'] = md5($req['password']);

        $users = DB::table('tb_users')
                    ->where('username',$req['username'])
                    ->where('password',$req['password'])
                    ->first();
        
        if(empty($users)){
            $status = 201;
            $msg = 'not found';
        }else{
            $status = 200;
            $msg = json_decode(json_encode($users), true);
        }

        // return $msg;
        return response()->json($msg, $status);
    }

    public function hapus($id)
    {
        $users = DB::table('tb_users')->where('id', $id)->delete();

        if($users){
            $status = 201;
            $msg = 'failed';
        }else{
            $status = 200;
            $msg = 'success';
        }       

        return response()->json($msg, $status); 
    }
}
