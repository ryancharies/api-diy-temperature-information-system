<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use DB;

class PasienController extends Controller
{
    public function data()
    {
        $pasien = DB::table('tb_pasien')->get();

        $data = (empty($pasien)) ? [] : $pasien;
        return $data;
    }

    public function dataPasienMasuk()
    {
        $pasien = DB::table('tb_pasien')->whereNull('tanggal_keluar')->get();

        $data = (empty($pasien)) ? [] : $pasien;
        return $data;
    }

    public function pasien($id)
    {
        $pasien = DB::table('tb_pasien')->where('id',$id)->first();

        $data = json_decode(json_encode($pasien), true);
        return $data;
    }

    public function register(Request $request)
    {
        $req = $request->json()->all();
        // return $req;

        if(!empty($req['id'])){
            $id = $req['id'];
            unset($req['id']);
            $pasien = DB::table('tb_pasien')->where('id',$id)->update($req);
        }else{
            unset($req['id']);
            $req['jam_masuk'] = $req['jam_masuk'].':00';
            $req['tanggal_keluar'] = NULL;
            $req['jam_keluar'] = NULL;
            $pasien = DB::table('tb_pasien')->insert($req);
        }
        
        $msg = ($pasien) ? 'success' : 'failed';

        return $msg;
    }

    public function hapus($id)
    {
        $pasien = DB::table('tb_pasien')->where('id', $id)->delete();

        if($pasien){
            $status = 201;
            $msg = 'failed';
        }else{
            $status = 200;
            $msg = 'success';
        }       

        return response()->json($msg, $status);         
    }

    public function ganti(Request $request)
    {
        $param = $request->json()->all();
        $pasien = DB::table('tb_pasien_aktif')
                        ->where('id', $param['no'])
                        ->update(['id_pasien' => $param['id']]);

        if(!$pasien){
            $status = 201;
            $msg = 'failed';
        }else{
            $status = 200;
            $msg = 'success';
        }       

        return response()->json($msg, $status);      
    }

    public function aktif()
    {
        $pasien = DB::table('tb_pasien_aktif')->where('id', 1)->value('id_pasien');
        return $pasien;
    }

    public function aktifkedua()
    {
        $pasien = DB::table('tb_pasien_aktif')->where('id', 2)->value('id_pasien');
        return $pasien;
    }

    public function catatan(Request $request)
    {
        $req = $request->json()->all();
        $req['tanggal'] = date("Y-m-d H:i:s");

        if(!empty(trim($req['catatan']))){
            $laporan = $req['laporan'];
            unset($req['laporan']);
            $pasien = DB::table('tb_pasien_catatan')->insert($req);
            $req['laporan'] = $laporan;
        }

        if(!empty(trim($req['laporan']))){
            unset($req['catatan']);
            $pasien = DB::table('tb_pasien_laporan')->insert($req);
        }
        
        $msg = ($pasien) ? 'success' : 'failed';

        return $msg;
    }

    public function dataSensor($id, $alat)
    {   
        $sensor = DB::table('tb_pasien_sensor')->where('id_pasien', $id)->where('id_alat', $alat)->orderBy('id','DESC')->first();
        $data = json_decode(json_encode($sensor), true);
        return $data;        
    }

    public function sensor(Request $request)
    {
        $req = $request->all();

        if($req['id_alat'] == 2){
            $last = DB::table('tb_pasien_sensor')->where('id_alat',1)->orderBy('tanggal','desc')->first();
            $req['suhu_ruang'] = $last->suhu_ruang;
            $req['kelembapan'] = $last->kelembapan;
        }

        $req['id_ruangan'] = 1;
        $req['tanggal'] = date('Y-m-d H:i:s');
        $req['id_pasien'] = DB::table('tb_pasien_aktif')->where('id',1)->value('id_pasien');

        $pasien = DB::table('tb_pasien_sensor')->insert($req);
        $msg = ($pasien) ? 'success' : 'failed';

        return $msg;
    }

    public function sensorById($id)
    {
        $sensor = DB::table('tb_pasien_sensor')->where('id_pasien', $id)->orderBy('tanggal','DESC')->get();
        return response()->json($sensor);
    }

    public function sensorChartById($id)
    {
        $sensor = DB::table('tb_pasien_sensor')
                            ->selectRaw('suhu_ruang as sr, suhu_tubuh as st, kelembapan as k, TIME(tanggal) as t')
                            ->where('id_pasien', $id)
                            ->whereDate('tanggal',date('Y-m-d'))
                            ->orderBy('tanggal','asc')
                            ->limit(10)
                            ->get();

        $label = array();
        $sr = array();
        $st = array();
        $k = array();
        foreach ($sensor as $value) {
            array_push($label, $value->t);
            array_push($sr, $value->sr);
            array_push($st, $value->st);
            array_push($k, $value->k);
        }

        $data = [
            'sr' => $sr,
            'st' => $st,
            'k'  => $k,
            't'  => $label
        ];
        return response()->json($data);
    }

    public function catatanById($id)
    {
        $catatan = DB::table('tb_pasien_catatan as c')
                        ->join('tb_users as u','c.id_users','=','u.id')
                        ->select('c.*','u.nama as user')
                        ->where('c.id_pasien', $id)
                        ->orderBy('c.tanggal','DESC')
                        ->get();

        return response()->json($catatan);
    }

    public function laporanById($id)
    {
        $laporan = DB::table('tb_pasien_laporan as c')
                        ->join('tb_users as u','c.id_users','=','u.id')
                        ->select('c.*','u.nama as user')
                        ->where('c.id_pasien', $id)
                        ->orderBy('c.tanggal','DESC')
                        ->get();

        return response()->json($laporan);
    }
}
