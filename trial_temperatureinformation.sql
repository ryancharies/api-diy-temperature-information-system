/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : trial_temperatureinformation

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 21/05/2019 14:19:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_akses
-- ----------------------------
DROP TABLE IF EXISTS `tb_akses`;
CREATE TABLE `tb_akses`  (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_akses
-- ----------------------------
INSERT INTO `tb_akses` VALUES (1, 'Dokter');
INSERT INTO `tb_akses` VALUES (2, 'Perawat');

-- ----------------------------
-- Table structure for tb_alat
-- ----------------------------
DROP TABLE IF EXISTS `tb_alat`;
CREATE TABLE `tb_alat`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_alat
-- ----------------------------
INSERT INTO `tb_alat` VALUES (1, 'DEVICE 00');
INSERT INTO `tb_alat` VALUES (2, 'DEVICE 01');

-- ----------------------------
-- Table structure for tb_pasien
-- ----------------------------
DROP TABLE IF EXISTS `tb_pasien`;
CREATE TABLE `tb_pasien`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `usia` int(2) NULL DEFAULT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `riwayat_penyakit` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_masuk` date NULL DEFAULT NULL,
  `jam_masuk` time(0) NULL DEFAULT NULL,
  `tanggal_keluar` date NULL DEFAULT NULL,
  `jam_keluar` time(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_pasien
-- ----------------------------
INSERT INTO `tb_pasien` VALUES (1, 'Ryan Charies', NULL, 24, 'L', 'Tidak ada', NULL, NULL, NULL, NULL);
INSERT INTO `tb_pasien` VALUES (2, 'Roki', NULL, 22, 'L', 'tidak ada', NULL, NULL, NULL, NULL);
INSERT INTO `tb_pasien` VALUES (3, 'Susi', NULL, 56, 'L', 'tidak ada', NULL, NULL, NULL, NULL);
INSERT INTO `tb_pasien` VALUES (4, 'Rizal', '2019-05-16', 45, 'L', 'tidak ada', '2019-05-16', '20:25:00', NULL, NULL);

-- ----------------------------
-- Table structure for tb_pasien_aktif
-- ----------------------------
DROP TABLE IF EXISTS `tb_pasien_aktif`;
CREATE TABLE `tb_pasien_aktif`  (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_pasien` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_pasien_aktif
-- ----------------------------
INSERT INTO `tb_pasien_aktif` VALUES (1, 1);
INSERT INTO `tb_pasien_aktif` VALUES (2, 2);

-- ----------------------------
-- Table structure for tb_pasien_catatan
-- ----------------------------
DROP TABLE IF EXISTS `tb_pasien_catatan`;
CREATE TABLE `tb_pasien_catatan`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pasien` int(3) NULL DEFAULT NULL,
  `id_users` int(3) NULL DEFAULT NULL,
  `id_ruangan` int(3) NULL DEFAULT NULL,
  `catatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_pasien_catatan
-- ----------------------------
INSERT INTO `tb_pasien_catatan` VALUES (1, 1, 4, 1, 'makan', '2019-05-21 13:54:31');
INSERT INTO `tb_pasien_catatan` VALUES (2, 1, 4, 1, 'makan', '2019-05-21 13:57:17');
INSERT INTO `tb_pasien_catatan` VALUES (3, 2, 4, 1, 'minum', '2019-05-21 14:00:30');
INSERT INTO `tb_pasien_catatan` VALUES (4, 2, 4, 1, 'makan saja lah', '2019-05-21 14:03:04');

-- ----------------------------
-- Table structure for tb_pasien_laporan
-- ----------------------------
DROP TABLE IF EXISTS `tb_pasien_laporan`;
CREATE TABLE `tb_pasien_laporan`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pasien` int(3) NULL DEFAULT NULL,
  `id_users` int(3) NULL DEFAULT NULL,
  `id_ruangan` int(3) NULL DEFAULT NULL,
  `laporan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_pasien_laporan
-- ----------------------------
INSERT INTO `tb_pasien_laporan` VALUES (1, 1, 4, 1, 'sudah dikasih makan', '2019-05-21 13:57:17');
INSERT INTO `tb_pasien_laporan` VALUES (2, 2, 4, 1, 'sudah dikasih minum', '2019-05-21 14:00:30');
INSERT INTO `tb_pasien_laporan` VALUES (3, 2, 4, 1, 'minum saja lah', '2019-05-21 14:03:17');

-- ----------------------------
-- Table structure for tb_pasien_sensor
-- ----------------------------
DROP TABLE IF EXISTS `tb_pasien_sensor`;
CREATE TABLE `tb_pasien_sensor`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pasien` int(3) NULL DEFAULT NULL,
  `id_ruangan` int(3) NULL DEFAULT NULL,
  `id_alat` int(3) NULL DEFAULT NULL,
  `suhu_ruang` double NULL DEFAULT NULL,
  `suhu_tubuh` double NULL DEFAULT NULL,
  `kelembapan` double NULL DEFAULT NULL,
  `tanggal` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 96 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_pasien_sensor
-- ----------------------------
INSERT INTO `tb_pasien_sensor` VALUES (1, 1, 1, 1, 25.3, 25.25, 99.9, '2019-04-30 07:01:55');
INSERT INTO `tb_pasien_sensor` VALUES (2, 1, 1, 1, 25.3, 25.25, 99.9, '2019-04-30 07:02:09');
INSERT INTO `tb_pasien_sensor` VALUES (3, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:02:20');
INSERT INTO `tb_pasien_sensor` VALUES (4, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:02:30');
INSERT INTO `tb_pasien_sensor` VALUES (5, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:02:41');
INSERT INTO `tb_pasien_sensor` VALUES (6, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:02:51');
INSERT INTO `tb_pasien_sensor` VALUES (7, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:03:02');
INSERT INTO `tb_pasien_sensor` VALUES (8, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:03:12');
INSERT INTO `tb_pasien_sensor` VALUES (9, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:03:22');
INSERT INTO `tb_pasien_sensor` VALUES (10, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:03:33');
INSERT INTO `tb_pasien_sensor` VALUES (11, 1, 1, 1, 25.4, 25.25, 99.9, '2019-04-30 07:03:43');
INSERT INTO `tb_pasien_sensor` VALUES (12, 1, 1, 1, 25.4, 25.31, 99.9, '2019-04-30 07:03:53');
INSERT INTO `tb_pasien_sensor` VALUES (13, 1, 1, 1, 25.5, 25.31, 99.9, '2019-04-30 07:04:04');
INSERT INTO `tb_pasien_sensor` VALUES (14, 1, 1, 2, 25.5, 25.94, 99.9, '2019-04-30 07:04:09');
INSERT INTO `tb_pasien_sensor` VALUES (15, 1, 1, 1, 25.5, 25.31, 99.9, '2019-04-30 07:04:14');
INSERT INTO `tb_pasien_sensor` VALUES (16, 1, 1, 2, 25.5, 25.88, 99.9, '2019-04-30 07:04:20');
INSERT INTO `tb_pasien_sensor` VALUES (17, 1, 1, 1, 25.5, 25.31, 99.9, '2019-04-30 07:04:24');
INSERT INTO `tb_pasien_sensor` VALUES (18, 1, 1, 2, 25.5, 26.06, 99.9, '2019-04-30 07:04:31');
INSERT INTO `tb_pasien_sensor` VALUES (19, 1, 1, 1, 25.5, 25.31, 99.9, '2019-04-30 07:04:35');
INSERT INTO `tb_pasien_sensor` VALUES (20, 1, 1, 2, 25.5, 25.88, 99.9, '2019-04-30 07:04:41');
INSERT INTO `tb_pasien_sensor` VALUES (21, 1, 1, 1, 25.5, 25.31, 99.9, '2019-04-30 07:04:45');
INSERT INTO `tb_pasien_sensor` VALUES (22, 1, 1, 2, 25.5, 26, 99.9, '2019-04-30 07:04:52');
INSERT INTO `tb_pasien_sensor` VALUES (23, 1, 1, 1, 25.6, 25.44, 99.9, '2019-04-30 07:04:56');
INSERT INTO `tb_pasien_sensor` VALUES (24, 1, 1, 2, 25.6, 26.06, 99.9, '2019-04-30 07:05:02');
INSERT INTO `tb_pasien_sensor` VALUES (25, 1, 1, 1, 25.5, 25.63, 99.9, '2019-04-30 07:05:06');
INSERT INTO `tb_pasien_sensor` VALUES (26, 1, 1, 2, 25.5, 26, 99.9, '2019-04-30 07:05:12');
INSERT INTO `tb_pasien_sensor` VALUES (27, 1, 1, 1, 25.5, 25.75, 99.9, '2019-04-30 07:05:16');
INSERT INTO `tb_pasien_sensor` VALUES (28, 1, 1, 2, 25.5, 25.94, 99.9, '2019-04-30 07:05:23');
INSERT INTO `tb_pasien_sensor` VALUES (29, 1, 1, 1, 25.6, 25.94, 99.9, '2019-04-30 07:05:27');
INSERT INTO `tb_pasien_sensor` VALUES (30, 1, 1, 2, 25.6, 25.94, 99.9, '2019-04-30 07:05:33');
INSERT INTO `tb_pasien_sensor` VALUES (31, 1, 1, 1, 25.6, 26, 99.9, '2019-04-30 07:05:37');
INSERT INTO `tb_pasien_sensor` VALUES (32, 1, 1, 2, 25.6, 26.06, 99.9, '2019-04-30 07:05:43');
INSERT INTO `tb_pasien_sensor` VALUES (33, 1, 1, 1, 25.6, 26.13, 99.9, '2019-04-30 07:05:48');
INSERT INTO `tb_pasien_sensor` VALUES (34, 1, 1, 2, 25.6, 26, 99.9, '2019-04-30 07:05:53');
INSERT INTO `tb_pasien_sensor` VALUES (35, 1, 1, 1, 25.6, 26.19, 99.9, '2019-04-30 07:05:58');
INSERT INTO `tb_pasien_sensor` VALUES (36, 1, 1, 2, 25.6, 25.94, 99.9, '2019-04-30 07:06:04');
INSERT INTO `tb_pasien_sensor` VALUES (37, 1, 1, 1, 25.6, 26.25, 99.9, '2019-04-30 07:06:09');
INSERT INTO `tb_pasien_sensor` VALUES (38, 1, 1, 1, 28.9, 28.56, 92.9, '2019-05-13 10:13:07');
INSERT INTO `tb_pasien_sensor` VALUES (39, 1, 1, 1, 28.9, 28.56, 93, '2019-05-13 10:13:16');
INSERT INTO `tb_pasien_sensor` VALUES (40, 1, 1, 1, 28.9, 28.69, 92.9, '2019-05-13 10:13:27');
INSERT INTO `tb_pasien_sensor` VALUES (41, 1, 1, 1, 28.9, 29.31, 92.9, '2019-05-13 10:13:37');
INSERT INTO `tb_pasien_sensor` VALUES (42, 1, 1, 1, 28.9, 29.44, 93.1, '2019-05-13 10:13:48');
INSERT INTO `tb_pasien_sensor` VALUES (43, 1, 1, 1, 28.9, 29.44, 93, '2019-05-13 10:13:58');
INSERT INTO `tb_pasien_sensor` VALUES (44, 1, 1, 1, 28.9, 29.38, 93, '2019-05-13 10:14:09');
INSERT INTO `tb_pasien_sensor` VALUES (45, 1, 1, 1, 28.9, 29.94, 93, '2019-05-13 10:14:19');
INSERT INTO `tb_pasien_sensor` VALUES (46, 1, 1, 1, 28.9, 31.38, 92.9, '2019-05-13 10:14:30');
INSERT INTO `tb_pasien_sensor` VALUES (47, 1, 1, 1, 28.9, 31.19, 92.9, '2019-05-13 10:14:40');
INSERT INTO `tb_pasien_sensor` VALUES (48, 1, 1, 1, 28.9, 30.88, 92.9, '2019-05-13 10:14:51');
INSERT INTO `tb_pasien_sensor` VALUES (49, 1, 1, 1, 28.9, 30.63, 92.9, '2019-05-13 10:15:02');
INSERT INTO `tb_pasien_sensor` VALUES (50, 1, 1, 1, 28.9, 30.44, 93.1, '2019-05-13 10:15:12');
INSERT INTO `tb_pasien_sensor` VALUES (51, 1, 1, 1, 28.9, 30.19, 93.1, '2019-05-13 10:15:22');
INSERT INTO `tb_pasien_sensor` VALUES (52, 1, 1, 1, 28.9, 30, 93, '2019-05-13 10:15:33');
INSERT INTO `tb_pasien_sensor` VALUES (53, 1, 1, 1, 28.9, 29.81, 93.1, '2019-05-13 10:15:43');
INSERT INTO `tb_pasien_sensor` VALUES (54, 1, 1, 1, 28.8, 29.69, 93, '2019-05-13 10:15:54');
INSERT INTO `tb_pasien_sensor` VALUES (55, 1, 1, 1, 28.8, 29.5, 93, '2019-05-13 10:16:05');
INSERT INTO `tb_pasien_sensor` VALUES (56, 1, 1, 1, 28.8, 29.31, 93, '2019-05-13 10:16:16');
INSERT INTO `tb_pasien_sensor` VALUES (57, 1, 1, 1, 28.8, 29.19, 93, '2019-05-13 10:16:26');
INSERT INTO `tb_pasien_sensor` VALUES (58, 1, 1, 1, 28.8, 29.06, 93, '2019-05-13 10:16:37');
INSERT INTO `tb_pasien_sensor` VALUES (59, 1, 1, 1, 28.8, 28.94, 93, '2019-05-13 10:16:48');
INSERT INTO `tb_pasien_sensor` VALUES (60, 1, 1, 1, 28.8, 28.81, 93, '2019-05-13 10:16:58');
INSERT INTO `tb_pasien_sensor` VALUES (61, 1, 1, 1, 28.8, 28.69, 93, '2019-05-13 10:17:08');
INSERT INTO `tb_pasien_sensor` VALUES (62, 1, 1, 1, 28.8, 28.56, 93.1, '2019-05-13 10:17:19');
INSERT INTO `tb_pasien_sensor` VALUES (63, 1, 1, 1, 28.8, 28.44, 93.1, '2019-05-13 10:17:30');
INSERT INTO `tb_pasien_sensor` VALUES (64, 1, 1, 1, 28.8, 28.38, 93.2, '2019-05-13 10:17:40');
INSERT INTO `tb_pasien_sensor` VALUES (65, 1, 1, 1, 28.8, 28.25, 93.2, '2019-05-13 10:17:51');
INSERT INTO `tb_pasien_sensor` VALUES (66, 1, 1, 1, 28.8, 28.19, 93.2, '2019-05-13 10:18:01');
INSERT INTO `tb_pasien_sensor` VALUES (67, 1, 1, 1, 28.8, 28.13, 93.2, '2019-05-13 10:18:12');
INSERT INTO `tb_pasien_sensor` VALUES (68, 1, 1, 1, 28.8, 28.06, 93.2, '2019-05-13 10:18:22');
INSERT INTO `tb_pasien_sensor` VALUES (69, 1, 1, 1, 28.8, 28, 93.2, '2019-05-13 10:18:33');
INSERT INTO `tb_pasien_sensor` VALUES (70, 1, 1, 1, 28.7, 27.94, 93.2, '2019-05-13 10:18:43');
INSERT INTO `tb_pasien_sensor` VALUES (71, 1, 1, 1, 28.7, 27.94, 93.2, '2019-05-13 10:18:54');
INSERT INTO `tb_pasien_sensor` VALUES (72, 1, 1, 1, 28.7, 27.88, 93.3, '2019-05-13 10:19:05');
INSERT INTO `tb_pasien_sensor` VALUES (73, 1, 1, 1, 28.7, 27.81, 93.3, '2019-05-13 10:19:15');
INSERT INTO `tb_pasien_sensor` VALUES (74, 1, 1, 1, 28.7, 27.81, 93.3, '2019-05-13 10:19:26');
INSERT INTO `tb_pasien_sensor` VALUES (75, 1, 1, 1, 28.7, 27.75, 93.3, '2019-05-13 10:19:37');
INSERT INTO `tb_pasien_sensor` VALUES (76, 1, 1, 1, 28.7, 27.69, 93.3, '2019-05-13 10:19:47');
INSERT INTO `tb_pasien_sensor` VALUES (77, 1, 1, 1, 28.7, 27.69, 93.3, '2019-05-13 10:19:58');
INSERT INTO `tb_pasien_sensor` VALUES (78, 1, 1, 1, 28.7, 27.63, 93.4, '2019-05-13 10:20:08');
INSERT INTO `tb_pasien_sensor` VALUES (79, 1, 1, 1, 28.7, 27.63, 93.4, '2019-05-13 10:20:19');
INSERT INTO `tb_pasien_sensor` VALUES (80, 1, 1, 1, 28.7, 27.63, 93.5, '2019-05-13 10:20:29');
INSERT INTO `tb_pasien_sensor` VALUES (81, 1, 1, 1, 28.1, 27.06, 95, '2019-05-13 17:40:02');
INSERT INTO `tb_pasien_sensor` VALUES (82, 1, 1, 1, 28.1, 27.06, 95, '2019-05-13 17:40:13');
INSERT INTO `tb_pasien_sensor` VALUES (83, 1, 1, 1, 28.1, 27.06, 94.9, '2019-05-13 17:40:23');
INSERT INTO `tb_pasien_sensor` VALUES (84, 1, 1, 1, 28.1, 27.06, 94.9, '2019-05-13 17:40:34');
INSERT INTO `tb_pasien_sensor` VALUES (85, 1, 1, 1, 28.2, 27.06, 94.9, '2019-05-13 17:40:44');
INSERT INTO `tb_pasien_sensor` VALUES (86, 1, 1, 1, 28.2, 27.06, 94.9, '2019-05-13 17:40:55');
INSERT INTO `tb_pasien_sensor` VALUES (87, 1, 1, 1, 28.2, 27.06, 94.9, '2019-05-13 17:41:05');
INSERT INTO `tb_pasien_sensor` VALUES (88, 1, 1, 1, 28.2, 27.06, 94.9, '2019-05-13 17:41:16');
INSERT INTO `tb_pasien_sensor` VALUES (89, 1, 1, 1, 28.2, 27.06, 94.9, '2019-05-13 17:41:26');
INSERT INTO `tb_pasien_sensor` VALUES (90, 1, 1, 1, 28.1, 27.06, 94.8, '2019-05-13 17:41:37');
INSERT INTO `tb_pasien_sensor` VALUES (91, 1, 1, 1, 28.1, 27.06, 94.7, '2019-05-13 17:41:47');
INSERT INTO `tb_pasien_sensor` VALUES (92, 1, 1, 1, 28.1, 27.06, 94.8, '2019-05-13 17:41:58');
INSERT INTO `tb_pasien_sensor` VALUES (93, 1, 1, 1, 28.2, 27.06, 94.8, '2019-05-13 17:42:08');
INSERT INTO `tb_pasien_sensor` VALUES (94, 1, 1, 1, 28.2, 27.06, 94.7, '2019-05-13 17:42:19');
INSERT INTO `tb_pasien_sensor` VALUES (95, 2, 1, 2, 26.8, 26.9, 93, '2019-05-13 17:40:55');

-- ----------------------------
-- Table structure for tb_ruangan
-- ----------------------------
DROP TABLE IF EXISTS `tb_ruangan`;
CREATE TABLE `tb_ruangan`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_ruangan
-- ----------------------------
INSERT INTO `tb_ruangan` VALUES (1, 'Ruangan 1');

-- ----------------------------
-- Table structure for tb_users
-- ----------------------------
DROP TABLE IF EXISTS `tb_users`;
CREATE TABLE `tb_users`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_akses` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_users
-- ----------------------------
INSERT INTO `tb_users` VALUES (1, 'Hamami', 'hamami', 'c4ca4238a0b923820dcc509a6f75849b', 1);
INSERT INTO `tb_users` VALUES (5, 'Administrator', 'admin', 'c4ca4238a0b923820dcc509a6f75849b', 0);
INSERT INTO `tb_users` VALUES (4, 'Roki', 'roki', 'c4ca4238a0b923820dcc509a6f75849b', 1);

SET FOREIGN_KEY_CHECKS = 1;
